const formulario = document.querySelector('#formulario-contacto');
const inputs = document.querySelectorAll('#formulario-contacto input');
const textarea = document.querySelector('#formulario-contacto textarea');

const expresiones = {
  nombre: /^[a-zA-ZñÑ\s]+$/,
  email: /^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/,
  mensaje: /^[0-9a-zA-ZñÑ\s]+$/
};

const campos = {
  nombre: false,
  email: false,
  mensaje: false
};

const validarFormularioInput = (e) => {
  //Accedemos al valor que tiene el id
  //console.log(e.target.id);

  switch(e.target.id){
    case 'nombre':
      validarCampoInput(expresiones.nombre, e.target, 'nombre');
      break;
    case 'email':
      validarCampoInput(expresiones.email, e.target, 'email');
      break;
  }
};

const validarCampoInput = (expresion, input, campo) => {

  if(expresion.test(input.value)){
    //Remueve la clase
    document.querySelector(`#grupo-${campo} .form-control`).classList.remove('form-control-error');
    document.querySelector(`#grupo-${campo} .formulario-input-error`).classList.remove('formulario-input-error-activo');
    campos[campo] = true;
  } else {
    //Adiciona la clase
    document.querySelector(`#grupo-${campo} .form-control`).classList.add('form-control-error');
    document.querySelector(`#grupo-${campo} .formulario-input-error`).classList.add('formulario-input-error-activo');
    campos[campo] = false;
  }
};

const validarFormularioTextArea = (e) => {
  //console.log(e.target.id);

  if(expresiones.mensaje.test(e.target.value)){
    //Remueve la clase
    document.querySelector('#grupo-mensaje #mensaje').classList.remove('form-control-error');
    document.querySelector('#grupo-mensaje .formulario-input-error').classList.remove('formulario-input-error-activo');
    campos['mensaje'] = true
  } else {
    //Adiciona la clase
    document.querySelector('#grupo-mensaje #mensaje').classList.add('form-control-error');
    document.querySelector('#grupo-mensaje .formulario-input-error').classList.add('formulario-input-error-activo');
    campos['mensaje'] = false;
  }
};

inputs.forEach((input) => {
  input.addEventListener('keyup', validarFormularioInput);
  input.addEventListener('blur', validarFormularioInput);
});

textarea.addEventListener('keyup', validarFormularioTextArea);

textarea.addEventListener('blur', validarFormularioTextArea);

$("#formulario-contacto").keyup(function() {
  if (campos.nombre && campos.email && campos.mensaje) {
    $("#submitBtn").removeAttr("disabled", "disabled");
   } else {
    $("#submitBtn").attr("disabled", "disabled");
  }    
});




formulario.addEventListener("submit", (e)=>{
  e.preventDefault();


  fetch("https://formsubmit.co/ajax/poliverdosantos@gmail.com",{
    method:"POST",
    body:new FormData(e.target)
  })
  .then(res=>res.ok? res.json(): Promise.reject(res))
  .then(json=>{
    console.log(json);
    formulario.reset();
    $("#submitBtn").attr("disabled", "disabled");
    document.getElementById('formulario-mensaje-exito').classList.add('formulario-mensaje-exito-activo');
		setTimeout(() => {
			document.getElementById('formulario-mensaje-exito').classList.remove('formulario-mensaje-exito-activo');
		}, 4000);
  })
  .catch(err=>{
    console.log(err);
    let message = err.statusText || "Ocurrio un error al enviar intenta nuevamente";
  });
})